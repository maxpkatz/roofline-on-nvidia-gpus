#define CPLX
#include "f_defs.h"
program sigma_gpp_gpu 
    use openacc
    use cudafor
    implicit none

    integer, parameter :: DP = kind(1.0d0)
    integer, parameter :: DPC = kind((1.0d0,1.0d0))
    integer, parameter :: size_of_complex_DPC= DPC * 2
#ifdef CPLX
    integer, parameter :: size_of_SCALAR = size_of_complex_DPC
#else
    integer, parameter :: size_of_SCALAR = DP
#endif

    integer, allocatable :: indinv(:)
    real(DP), allocatable :: vcoul(:) !< (ncoul)
    integer, allocatable :: inv_igp_index(:) !< (neps)
    SCALAR, allocatable :: aqsntemp(:,:),aqsmtemp(:,:)
    integer :: ipe, ispin, ngpown, ncouls, nstart, nend, sig_fdf, n1true, indigp, igp, iw
    SCALAR :: Omega2, wtilde2
    SCALAR, allocatable :: asxtemp(:),achtemp(:)
    SCALAR, allocatable :: asxtempref(:),achtempref(:)
    SCALAR, allocatable :: I_eps_array(:,:)
    complex(DPC) :: wtilde
    complex(DPC), allocatable :: wtilde_array(:,:)

    SCALAR, allocatable :: aqsmtemp_local(:,:)
    real(DP) :: delwr, delw2, wdiffr, rden, ssxcutoff, limitone, limittwo
    complex(DPC) :: halfinvwtilde, delw, wdiff, cden
    real(DP) :: cden_mod_square
    SCALAR :: ssx_array_1, ssx_array_2, ssx_array_3, sch_array_1, sch_array_2, sch_array_3
    integer :: igbeg, igend, igblk
    SCALAR :: ssx, sch, schtt, matngmatmgp, epsa, aqsmconj
    integer :: ntband_dist
    real(DP), allocatable :: occ_array(:)
    real(DP), allocatable :: wx_array_t(:,:)
    integer, allocatable :: n1true_array(:)
    integer :: nvband, ncrit
    real(DP) :: efermi, tol, sexcut
    real(DP) :: wxt, vcoulx, vcoulxocc, e_n1kq
    SCALAR :: upd1, upd2
    real(DP), allocatable :: vcoul_loc(:)
    integer(kind=8) ::  my_igp, n1_loc, ig
    integer :: cuErr
    integer(kind=cuda_count_kind) :: freemem, totmem
    integer   :: stat
    integer wta_blk, wta_blks, wta_blke, wta_blksize, wta_blknum, wta_blksize_last
    integer :: my_igp_blk, my_igp_s,my_igp_e,my_igp_blksize,my_igp_blknum, my_igp_blklast
    integer :: veclen,gangnum,n1temp, totblknum,streami,outernum
    integer :: ig_blksize, ig_blk, ig_blknum, ig_s, ig_e, ig_blklast
    integer :: n1loc_blksize, n1loc_blk, n1loc_blknum, n1loc_s, n1loc_e, n1loc_blklast
!    SCALAR, allocatable :: ssx_array_cy(:,:,:), sch_array_cy(:,:,:)
    SCALAR, allocatable :: ssx_array_cy(:,:), sch_array_cy(:,:)
    integer :: peinf_ntband_dist,gvec_ng,ncoul,epsmpi_ngpown_max,peinf_ntband_max,sig_ntband
    integer,allocatable :: peinf_indext_dist(:)
    real(DP),allocatable :: wfnkq_ekq(:)
    real(DP), parameter :: TOL_Small = 1.0d-6
    real(DP), parameter :: TOL_Zero = 1.0d-12
    real(DP) :: sig_gamma, e_lk, sig_dw
    real :: start, finish
    character inputfile*100
    character debug

    call setvbuf3f(6,2,0)


    call getarg(1,inputfile)
    call getarg(2,debug)
    open(unit=66, file=inputfile, status='old', form='unformatted')
    read(unit=66) sig_ntband,gvec_ng,ncoul,epsmpi_ngpown_max,peinf_ntband_max
    read(unit=66) peinf_ntband_dist,ipe,ispin,ngpown,ncouls

    if (debug .eq. '1') then
    write(6,*) 'sig_ntband,gvec_ng,ncoul,epsmpi_ngpown_max,peinf_ntband_max'
    write(6,*) sig_ntband,gvec_ng,ncoul,epsmpi_ngpown_max,peinf_ntband_max
    write(6,*) 'peinf_ntband_dist,ipe,ispin,ngpown,ncouls'
    write(6,*) peinf_ntband_dist,ipe,ispin,ngpown,ncouls
    endif

    allocate(peinf_indext_dist(peinf_ntband_max))
    allocate(wfnkq_ekq(sig_ntband))
    allocate(inv_igp_index(epsmpi_ngpown_max)) !< (neps)
    allocate(indinv(gvec_ng))
    allocate(aqsmtemp(ncouls,peinf_ntband_max))
    allocate(vcoul(ncoul))
    allocate(wtilde_array(ncouls,ngpown))
    allocate(I_eps_array(ncouls,ngpown))
    allocate(aqsntemp(ncouls,peinf_ntband_max))
    allocate(asxtemp(3))
    allocate(achtemp(3))
    allocate(asxtempref(3))
    allocate(achtempref(3))

!    write(6,*) size(peinf_indext_dist),size(wfnkq_ekq)
!    write(6,*) size(inv_igp_index),size(indinv) 
!    write(6,*) size(aqsmtemp,1),size(aqsmtemp,2),size(vcoul)
!    write(6,*) size(wtilde_array,1),size(wtilde_array,2),size(I_eps_array,1),size(I_eps_array,2)
    if (size(peinf_indext_dist) .ne. peinf_ntband_max .or. &
            size(wfnkq_ekq) .ne. sig_ntband .or. &
            size(inv_igp_index) .ne. epsmpi_ngpown_max .or. &
            size(indinv) .ne. gvec_ng .or. &
            size(aqsmtemp) .ne. (ncouls*peinf_ntband_max) .or. &
            size(vcoul) .ne. ncoul .or. &
            size(wtilde_array) .ne. (ncouls*ngpown) .or. &
            size(I_eps_array) .ne. (ncouls*ngpown) .or. &
            size(aqsntemp) .ne. (ncouls*peinf_ntband_max)) then
            write(6,*) 'Error in reading'
    endif

    read(unit=66) peinf_indext_dist(:)
    read(unit=66) wfnkq_ekq(:)
    read(unit=66) nstart,nend,sig_fdf,sig_dw,sig_gamma,e_lk
    read(unit=66) inv_igp_index(:)
    read(unit=66) gvec_ng,indinv(:)
    read(unit=66) aqsmtemp(:,:)
    read(unit=66) nvband,tol,efermi
    read(unit=66) vcoul(:)
    read(unit=66) sexcut,limittwo,limitone
    read(unit=66) wtilde_array(:,:)
    read(unit=66) I_eps_array(:,:)
    read(unit=66) aqsntemp(:,:)
    read(unit=66) asxtemp(:)
    read(unit=66) achtemp(:)

    if (debug .eq. '1') then
    write(6,*) 'nstart,nend,sig_fdf,sig_dw,sig_gamma,e_lk'
    write(6,*) nstart,nend,sig_fdf,sig_dw,sig_gamma,e_lk
    write(6,*) 'nvband,tol,efermi'
    write(6,*) nvband,tol,efermi
    write(6,*) 'sexcut,limittwo,limitone'
    write(6,*) sexcut,limittwo,limitone
    write(6,*) 'read - before:'
    write(6,*) asxtemp(:)
    write(6,*) achtemp(:)
    endif

!$ACC DATA COPYIN(I_eps_array,aqsntemp)
    call cpu_time(start)
    ! Some constants used in the loop below, computed here to save
    ! floating point operations
    limitone=1D0/(TOL_Small*4D0)
    limittwo=sig_gamma**2

    SAFE_ALLOCATE(wx_array_t,(peinf_ntband_dist,3))

    SAFE_ALLOCATE(aqsmtemp_local, (peinf_ntband_dist,ngpown))
    aqsmtemp_local = ZERO

    do n1_loc = 1, peinf_ntband_dist !(ipe)
      ! n1true = "True" band index of the band n1 w.r.t. all bands
      n1true = peinf_indext_dist(n1_loc) !,ipe) ! changed to input
      ! energy of the |n1,k-q> state
      e_n1kq = wfnkq_ekq(n1true) !,ispin)
      do iw=nstart,nend
        if (sig_fdf .eq. -3) then
!          wx_array_t(n1_loc,iw) = wxi(iw) - e_n1kq
        else
          wx_array_t(n1_loc,iw) = e_lk + sig_dw*(iw-2) - e_n1kq
        endif
        if (abs(wx_array_t(n1_loc,iw)) .lt. TOL_Zero) wx_array_t(n1_loc,iw) = TOL_Zero
      enddo
    enddo

      ! fill the aqsmtemp_local array
      do my_igp = 1,  ngpown
    do n1_loc = 1, peinf_ntband_dist !(ipe)
        indigp = inv_igp_index(my_igp)
        igp = indinv(indigp)
        if (igp .le. ncouls .and. igp .gt. 0) then
          aqsmtemp_local(n1_loc,my_igp) = aqsmtemp(igp,n1_loc)
        end if
    enddo
      end do 

    ntband_dist = peinf_ntband_dist !(ipe)

    SAFE_ALLOCATE(n1true_array, (ntband_dist))
    SAFE_ALLOCATE(occ_array, (ntband_dist))

    ! variables
!    nvband = sig%nvband
!    ncrit  = sig%ncrit
!    efermi = sig%efermi
!    tol    = sig%tol
!    sexcut = sig%sexcut

    do n1_loc = 1, ntband_dist
      n1true_array(n1_loc) = peinf_indext_dist(n1_loc) !,ipe)
      e_n1kq = wfnkq_ekq(n1true_array(n1_loc)) !,ispin)
      occ_array(n1_loc) = 0.0d0
      if (peinf_indext_dist(n1_loc) .le. nvband) then
        if (abs(e_n1kq-efermi)<tol) then
          occ_array(n1_loc) = 0.5d0 ! Fermi-Dirac distribution = 1/2 at Fermi level
        else
          occ_array(n1_loc) = 1.0d0
        endif
      endif
    enddo

    SAFE_ALLOCATE(vcoul_loc, (ngpown))
    vcoul_loc = 0.0_dp
    do my_igp = 1,  ngpown
      indigp = inv_igp_index(my_igp)
      igp = indinv(indigp)
      if (igp .le. ncouls .and. igp .gt. 0) then
        vcoul_loc(my_igp) = vcoul(igp)
      end if
    end do

!    if (peinf%inode .eq. 0) write(6,*) 'ntband_dist,nstart,nend',ntband_dist,nstart,nend

! This is the Outter loop

    stat = cudaMemGetInfo(freemem, totmem)
    wta_blksize = (freemem * 0.7 - ntband_dist * (sizeof(n1true_array(1)) + DP*2 + 3*DP +size_of_SCALAR*ngpown)-ngpown*DP) / (ncouls * DPC * 2) 

    if (wta_blksize .gt. ngpown) wta_blksize = ngpown
    if (wta_blksize .le. 0) wta_blksize = 1
    wta_blknum = ngpown / wta_blksize
    wta_blksize_last = mod(ngpown,wta_blksize)
    if (wta_blksize_last .gt.0) wta_blknum=wta_blknum+1
   ! if (peinf%inode .eq. 0) 
    if (debug .eq. '1') then
    write(6,*) 'wtilde_array_blksize,last,blknum',wta_blksize,wta_blksize_last,wta_blknum
    endif

!$ACC DATA COPYIN(n1true_array,occ_array,wx_array_t,aqsmtemp_local,vcoul_loc,wtilde_array,ssx_array_cy,sch_array_cy) !,I_eps_array,aqsntemp)

    do wta_blk = 1, wta_blknum
      wta_blks=(wta_blk-1)*wta_blksize+1
      wta_blke=min(wta_blk*wta_blksize,ngpown)
!     wta_blks=(wta_blk-1)*wta_blksize+1
!     wta_blke=wta_blk*wta_blksize
!     if (wta_blk.eq.wta_blknum .and. wta_blksize_last.gt.0) then
!        wta_blke=ngpown
!     endif
!$ACC DATA COPYIN(wtilde_array(:,wta_blks:wta_blke))



     do iw=nstart,nend
     !  ssx_array_1 = ZERO
     !  sch_array_1 = ZERO
     !  ssx_array_2 = ZERO
     !  sch_array_2 = ZERO
       ssx_array_3 = ZERO
       sch_array_3 = ZERO
!$ACC PARALLEL PRESENT(I_eps_array, aqsntemp) vector_length(512)
!$ACC LOOP GANG VECTOR reduction(+:ssx_array_1,sch_array_1,ssx_array_2,sch_array_2,ssx_array_3,sch_array_3) collapse(2)
      do my_igp = wta_blks, wta_blke
        do ig = 1, ncouls
          wtilde = wtilde_array(ig,my_igp)
          wtilde2 = wtilde**2
          epsa    = I_eps_array(ig,my_igp)
          Omega2 = wtilde2 * epsa
          ssxcutoff = sexcut * abs(epsa)
          vcoulx = vcoul_loc(my_igp)
!$ACC LOOP SEQ
          do n1_loc = 1, ntband_dist
            aqsmconj = MYCONJG(aqsmtemp_local(n1_loc,my_igp))
            matngmatmgp =  aqsmconj * aqsntemp(ig,n1_loc)
            vcoulxocc = vcoulx * occ_array(n1_loc)

            wxt = wx_array_t(n1_loc,iw)
            wdiff = wxt - wtilde
            wdiffr = wdiff*CONJG(wdiff)
            rden = 1d0 / wdiffr
            delw = wtilde*CONJG(wdiff)*rden
!            delw = wtilde / wdiff
            delwr = delw*CONJG(delw)
  
              sch = 0.0d0
              ssx = 0.0d0
            if (wdiffr.gt.limittwo .and. delwr.lt.limitone) then
              sch = delw * epsa
              cden = wxt**2 - wtilde2
              rden = cden*CONJG(cden)
              rden = 1D0 / rden
              ssx = Omega2 * CONJG(cden) * rden
!              ssx = Omega2 / cden
            else if ( delwr .gt. TOL_Zero) then
!              sch = 0.0d0
              cden = (4.0d0 * wtilde2 * (delw + 0.5D0 ))
              rden = cden*CONJG(cden)
              rden = 1D0 / rden
              ssx = -Omega2 * CONJG(cden) * rden * delw
!              ssx = -Omega2 * delw / cden
            endif

            upd1 = 0.0d0
            if (abs(ssx) .le. ssxcutoff .or. wxt .ge. 0.0d0) then
              upd1 = vcoulxocc * ssx * matngmatmgp
            endif
!            if (abs(ssx) .gt. ssxcutoff .and. wxt .lt. 0.0d0) then
!              upd1 = 0.0d0
!            else
!              upd1 = vcoulxocc * ssx * matngmatmgp
!            end if

            upd2 = vcoulx * sch * matngmatmgp * 0.5d0

            ssx_array_3 = ssx_array_3 + upd1
            sch_array_3 = sch_array_3 + upd2

          enddo ! loop over n1_loc
        enddo ! loop over g
        ! NEED FIX
        ! acht_n1_loc(n1true_array(n1_loc)) = acht_n1_loc(n1true_array(n1_loc)) + sch_array_1 * vcoul_loc(my_igp)
      enddo ! igp
!$ACC END PARALLEL 

      if (iw.eq.1) then
        ssx_array_1 = ssx_array_3
        sch_array_1 = sch_array_3
        ssx_array_3 = ZERO
        sch_array_3 = ZERO
      else if (iw.eq.2) then
        ssx_array_2 = ssx_array_3
        sch_array_2 = sch_array_3
        ssx_array_3 = ZERO
        sch_array_3 = ZERO
      end if

    end do ! iw


!$ACC END DATA
    enddo 
!$ACC END DATA

    asxtemp(1) = asxtemp(1) - ssx_array_1
    asxtemp(2) = asxtemp(2) - ssx_array_2
    asxtemp(3) = asxtemp(3) - ssx_array_3

    achtemp(1) = achtemp(1) + sch_array_1
    achtemp(2) = achtemp(2) + sch_array_2
    achtemp(3) = achtemp(3) + sch_array_3

   if (debug .eq. '2') then
   write(6,*) 'calculated:'
   write(6,*) asxtemp(nstart:nend)
   write(6,*) achtemp(nstart:nend)
   endif

   read(unit=66) asxtempref(:)
   read(unit=66) achtempref(:)
   close(unit=66)
   if (debug .eq. '2') then
   write(6,*) 'read - after:'
   write(6,*) asxtempref(nstart:nend)
   write(6,*) achtempref(nstart:nend)
   endif
   if ((abs(asxtempref(nstart)-asxtemp(nstart))+abs(asxtempref(nend)-asxtemp(nend))) .lt. TOL_Zero) then
           write(6,*) 'asxtemp correct!'
   endif
   if ((abs(achtempref(nstart)-achtemp(nstart))+abs(achtempref(nend)-achtemp(nend))) .lt. TOL_Zero) then
           write(6,*) 'achtemp correct!'
   endif

   SAFE_DEALLOCATE(wx_array_t)
   SAFE_DEALLOCATE(n1true_array)
   SAFE_DEALLOCATE(vcoul_loc)
   SAFE_DEALLOCATE(aqsmtemp_local)

!   deallocate(ssx_array_cy)
!   deallocate(sch_array_cy)
   deallocate(peinf_indext_dist)
   deallocate(wfnkq_ekq)
   deallocate(inv_igp_index)
   deallocate(indinv)
   deallocate(aqsmtemp)
   deallocate(vcoul)
   deallocate(wtilde_array)
   deallocate(asxtemp)
   deallocate(achtemp)
   deallocate(asxtempref)
   deallocate(achtempref)

   call cpu_time(finish)
   print '("Time = ",f6.3," seconds.")',finish-start
   !$ACC END DATA
   deallocate(I_eps_array)
   deallocate(aqsntemp)

end program sigma_gpp_gpu


