The general workflow to use these scripts is
1. [ ] `./run.prof`   to collect timing, FLOPs and Bytes information for a kernel
2. [ ] `./process.py` to process the results; `process.py` generates AI and FLOP/s data, calls `roofline.py` which plots Rooflines in `.png` and `.eps`. 

`run.profiles` can be used to collect Nsight Compute profiles as well (which can then be opened in GUI). 

These scripts streamline the data collection process for multiple job runs (multiple versions of code, multiple parameters, etc), but some simpler examples are provided in ExamplePlots/ for ploting just one Roofline (DRAM or hierarchical).
